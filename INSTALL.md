# Installing OddVoices

Install [Git LFS](https://git-lfs.github.com/) and make sure you are up to date:

    git lfs pull

Obtain submodules:

    git submodule update --init

Set up Python virtualenv:

    # Linux/macOS:
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install .

On Windows, substitute `python3` with `py -3`.

Analyze segments in `voices/cicada` directory and generate the voice file at `cicada.voice`:

    oddvoices-compile voices/cicada cicada.voice

You can compile other voices too with similar commands (e.g. `oddvoices-compile voices/air air.voice`).

Compile `sing` executable:

    # Linux/macOS:
    cmake -S cpp -B cpp/build -DCMAKE_BUILD_TYPE=Release
    cmake --build cpp/build

    # Windows:
    # For the -G option, use cmake --help and scroll down to the list of
    # Visual Studio versions. Find the string that matches the one you currently
    # have installed.
    cmake -S cpp -B cpp/build -G "Visual Studio 16 2019"
    cmake --build cpp/build --config Release

Sing a MIDI file with embedded lyrics:

    # Linux/macOS:
    ./cpp/build/sing cicada.voice cmudict-0.7b example/frere_jacques.mid out.wav

    # Windows:
    ./cpp/build/Release/sing.exe cicada.voice cmudict-0.7b example/frere_jacques.mid out.wav

Or supply lyrics in a separate file `alternate_lyrics.txt`:

    # Linux/macOS:
    ./cpp/build/sing cicada.voice cmudict-0.7b example/frere_jacques.mid -l alternate_lyrics.txt out.wav

    # Windows:
    ./cpp/build/Release/sing.exe cicada.voice cmudict-0.7b example/frere_jacques.mid -l alternate_lyrics.txt out.wav

See [docs/usage.md](docs/usage.md) for more detailed usage info.
