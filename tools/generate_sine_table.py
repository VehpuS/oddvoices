import numpy as np

if __name__ == "__main__":
    N = 256
    parts = []
    for x in np.sin(np.arange(N) * 2 * np.pi / N):
        parts.append(str(x))
    print(", ".join(parts))
