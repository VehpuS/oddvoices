# Creating a voice for OddVoices

This document contains full instructions for creating a new voice for OddVoices.

## Policy on voice inclusion

Thank you for your interest in creating a new voice for OddVoices! Sadly, we can't guarantee that every contributed voice can be included in the main distribution of OddVoices, but feel free to open an issue in this repo to announce your creation.

## Singing

It takes around 30 minutes to an hour of singing to produce a corpus.

The singer needs to be able to hold a note within a semitone without artifacts, as well as enunciate diphthongs the proper way (by holding the first vowel and quickly transitioning to the second). Formal training is recommended, as proper breathing will help with stamina and will produce a clearer and bolder voice.

See `wordlist/instructions.pdf` for the full instructions to be distributed to singers, and `wordlist/words.pdf` for the list of words.

## Tagging the corpus

At this stage, ensure the audio file is mono, has a sample rate of 48 kHz, and is encoded as a 16-bit linear PCM WAV file. Save it in the voice's directory as `audio.wav`.

Open up the audio file in Audacity. (On Linux, be sure to use JACK and not ALSA or PulseAudio. Audacity lies about the endpoints during audio playback!)

Each word in the wordlist has light gray text indicating which X-SAMPA diphone or monophone should be extracted. There may be multiple diphones or monophones for a single word, which are separated by commas.

In Audacity, drag to select a segment of time corresponding to the X-SAMPA diphone or monophone, and use Edit -> Labels -> Add Label at Selection to add a label. The label's text should be the X-SAMPA string.

Err on the side of shorter: it is better for intelligibility to tag too short than too long. This is especially true if one of the phonemes in the diphone is silence, `/_/` -- such diphones should have little to no actual silence.

If the diphone contains a plosive (stop), the tagged region should only include the transient if the plosive is the *second* phone in the diphone. For example, `Ak` will contain the transient for /k/, but `kA` starts in the middle of the /k/ and therefore only contains the puff of air that happens between /k/ and /A/. If this convention is not followed, then doubled plosives will be audible during singing.

You will encounter some isolated vowels, which are labeled "(long)" in the word list. If the isolated vowel is /A/, this will contain `_A`, `A_`, and the vowel itself as a monophone, `A`. For the latter, make the monophone as long as possible without including the onset from silence or fade out to silence.

Diphthongs require special care. A correctly sung diphthong has two parts: a long stable region and a short transition region. There are two rules to remember:

1. When tagging a diphthong as a monophone, tag *only* the stable region.
2. If a diphone has a diphthong as its first phoneme, the tagged area should include the transition region.

Once you have all your labels, use File -> Export -> Export Labels... and export to a file called `labels.txt`, in the same directory as `audio.wav`.

## Creating the database JSON

The final step is creating a `database.json` file, containing some metadata about your voice. This currently has only one parameter:

    {
        "f0_midi_note": 60
    }

where `f0_midi_note` is the MIDI note number of the pitch at which all notes are sung.

That's it! With the three files (audio file, label file, metadata file) you are ready to compile your voice to a `.voice` file. Instructions for that can be found in INSTALL.md at the root of this repository.

## Re-tagging

Sometimes it is necessary to jump into an existing voice and adjust a label. It can be a bit annoying to have to hunt around for the label you want. To partially remedy this, OddVoices provides an `oddvoices-locate` Python script (it will be in your PATH when the virtual environment is activated), which takes as arguments the path to the voice directory and the name of the segment you wish to locate. For example, to find the segment `_oU`:

    $ oddvoices-locate voices/quake/ _oU
    20:26.77 - 20:27.03

This gives you the timestamp for the given segment. It isn't an ideal user experience, but it's enough for my needs.
