PSOLA
-----

- Dutoit, T. et al. 1996. "The MBROLA project: Towards a set of high quality speech synthesizers free of use for non commercial purposes."
- Stylianou, Yannis. 2001. "Removing Linear Phase Mismatches in Concatenative Speech Synthesis."
- Fuks, Leonardo. 1999?. "Computer-aided musical analysis of extended vocal techniques for compositional applications."

Pitch detection
---------------

- Rabiner, Lawrence R. 1977. "On the Use of Autocorrelation Analysis for Pitch Detection."

Pitch contours
--------------

- Berndtsson, Gunilla. 1995. "The KTH rule system for singing synthesis."
- Cook, Perry. 1991. *Identification of Control Parameters in an Articulatory Vocal Tract Model, with Applications to Synthesis of Singing.* PhD dissertation.
- Lai, Wen-Tsing. 2009. "An F0 Contour Fitting Model for Singing Synthesis."

Textbooks
---------

- Dutoit, Thierry. 1997. *An Introduction to Text-to-Speech Synthesis.*
- Taylor, Paul. 2009. *Text-to-Speech Synthesis.*
