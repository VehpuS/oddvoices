import setuptools

setuptools.setup(
    name="oddvoices",
    version="0.0.2",
    license="Apache License",
    description="Python component of OddVoices, a speech synthesizer.",
    author="Nathan Ho",
    packages=setuptools.find_packages("python"),
    package_dir={"": "python"},
    python_requires=">=3.7",
    install_requires=["numpy", "scipy", "soundfile"],
    extras_require={
        "dev": ["pytest", "mypy", "black", "pre-commit"],
    },
    entry_points={
        "console_scripts": [
            "oddvoices-compile = oddvoices.corpus:main",
            "oddvoices-locate = oddvoices.corpus:locate_segment",
            "oddvoices-generate-wordlist = oddvoices.phonology:generate_wordlist",
        ],
    },
)
