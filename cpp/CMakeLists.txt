cmake_minimum_required(VERSION 3.18)

project(oddvoices)

set(CMAKE_CXX_STANDARD 17)

include_directories(src)
include_directories(external_libraries/midifile/include)
include_directories(external_libraries/rapidjson/include)

file(GLOB source_files src/*.cpp src/*.hpp)
file(GLOB test_files test/*.cpp test/*.hpp)
file(GLOB midifile_files external_libraries/midifile/src/*.cpp)
add_executable(oddvoices_test ${source_files} ${test_files})

set(frontend_files
    frontend/sing.cpp
    frontend/sing.hpp
    frontend/wav.cpp
    frontend/wav.hpp
)
add_library(
    oddvoices_frontend
    ${source_files}
    ${midifile_files}
    ${frontend_files}
)

add_executable(sing frontend/main.cpp)
target_link_libraries(sing oddvoices_frontend)

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror")
endif()
