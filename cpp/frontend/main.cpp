#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <sstream>
#include <vector>

#include "../src/liboddvoices.hpp"
#include "../src/g2p.hpp"
#include "sing.hpp"


void printUsage()
{
    std::cout << "Usage:" << std::endl;
    std::cout << "    sing VOICE_FILE CMUDICT OUT_WAV" << std::endl;
    std::cout << "        [-j JSON_FILE] [-m MIDI_FILE] [-l LYRICS_FILE]" << std::endl;
}


int main(int argc, char** argv)
{
    // I looked into incorporating an argument parser library, but none of
    // the options looked appealing, and our use case is so simple that it's
    // easier to roll our own than add a new dependency.

    bool noMoreOptionalArguments = false;
    std::vector<std::string> positionalArguments;
    std::map<std::string, std::string> optionalArguments;
    for (int i = 1; i < argc; i++) {
        std::string part = argv[i];
        if (part == "--") {
            noMoreOptionalArguments = true;
        } else if (part[0] == '-' && !noMoreOptionalArguments) {
            i += 1;
            if (i >= argc) {
                std::cerr << "Unexpected end of arguments" << std::endl;
                return 1;
            }
            std::string part2 = argv[i];
            optionalArguments[part] = part2;
        } else {
            positionalArguments.push_back(part);
        }
    }

    std::optional<std::string> lyricsFile;
    std::optional<std::string> midiFile;
    std::optional<std::string> jsonFile;

    for (auto const& [key, value] : optionalArguments) {
        if (key == "-j" || key == "--json") {
            jsonFile = value;
        } else if (key == "-m" || key == "--midi") {
            midiFile = value;
        } else if (key == "-l" || key == "--lyrics") {
            lyricsFile = value;
        } else {
            std::cerr << "Unrecognized argument " << key << std::endl;
            return 1;
        }
    }

    if (positionalArguments.size() < 3) {
        printUsage();
        return 1;
    }

    if (!(jsonFile.has_value() || midiFile.has_value())) {
        std::cerr << "You must provide either a JSON (-j) or MIDI (-m) file." << std::endl;
        return 1;
    }
    if (jsonFile.has_value() && midiFile.has_value()) {
        std::cerr << "You cannot provide both JSON and MIDI files." << std::endl;
        return 1;
    }

    auto& voiceFile = positionalArguments[0];
    auto& cmuDictFile = positionalArguments[1];
    auto& outWAVFile = positionalArguments[2];

    std::string lyrics = "";
    if (lyricsFile.has_value()) {
        std::ifstream stream(lyricsFile.value());
        if (!stream.good()) {
            std::cerr << "Failed opening lyrics file " << lyricsFile.value() << std::endl;
            return 1;
        }
        std::ostringstream stringStream;
        stringStream << stream.rdbuf();
        lyrics = stringStream.str();
    }

    oddvoices::Voice voice;
    voice.initFromFile(voiceFile);
    oddvoices::g2p::G2P g2p(cmuDictFile);
    if (jsonFile.has_value()) {
        std::ifstream stream(jsonFile.value());
        if (!stream.good()) {
            std::cerr << "Failed opening JSON file " << jsonFile.value() << std::endl;
            return 1;
        }
        std::ostringstream stringStream;
        stringStream << stream.rdbuf();
        std::string json = stringStream.str();
        auto [ok, message] = oddvoices::frontend::singJSON(voice, g2p, json, outWAVFile, lyrics);
        if (!ok) {
            std::cerr << message << std::endl;
            return 1;
        }
    } else {
        auto [ok, message] = oddvoices::frontend::singMIDI(voice, g2p, midiFile.value(), outWAVFile, lyrics);
        if (!ok) {
            std::cerr << message << std::endl;
            return 1;
        }
    }
    return 0;
}
