import pathlib
import subprocess

import numpy as np
import pytest
import soundfile

import oddvoices.corpus

ROOT = pathlib.Path(__file__).resolve().parent.parent
CMUDICT = ROOT / "cmudict-0.7b"
EXAMPLE_MIDI_FILE = ROOT / "example" / "frere_jacques.mid"
EXAMPLE_VOICE_DIR = ROOT / "voices" / "quake"

try:
    HAVE_VALGRIND = subprocess.run(["valgrind", "--version"]).returncode == 0
except FileNotFoundError:
    HAVE_VALGRIND = False

TEMP_DIR = ROOT / ".tmp"
TEMP_DIR.mkdir(exist_ok=True)


@pytest.fixture
def sing(pytestconfig):
    result = pytestconfig.getoption("--sing-executable")
    if result is None:
        raise RuntimeError(
            "Missing --sing-executable pytest option. "
            "Please point it to the sing executable."
        )
    return result


@pytest.fixture(scope="module")
def quake_voice():
    segment_database = oddvoices.corpus.CorpusAnalyzer(
        EXAMPLE_VOICE_DIR
    ).render_database()
    file_name = TEMP_DIR / "quake.voice"
    with open(file_name, "wb") as f:
        oddvoices.corpus.write_voice_file(f, segment_database)
    return file_name


def test_sing(quake_voice, sing):
    """Singing an input MIDI file results in non-silent audio."""
    out_file = TEMP_DIR / "out.wav"
    subprocess.run(
        [sing, quake_voice, CMUDICT, "-m", EXAMPLE_MIDI_FILE, out_file], check=True
    )
    audio, sample_rate = soundfile.read(out_file)
    assert any(audio != 0)


@pytest.mark.skipif(not HAVE_VALGRIND, reason="no Valgrind")
def test_valgrind(quake_voice, sing):
    """Running valgrind returns no errors."""
    out_file = TEMP_DIR / "out.wav"
    subprocess.run(
        ["valgrind", sing, quake_voice, CMUDICT, "-m", EXAMPLE_MIDI_FILE, out_file],
        check=True,
    )
